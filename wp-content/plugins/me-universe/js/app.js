var App = 
{
	submitNewUser: function()
	{
		var data = 
		{
			full_name: jQuery("form[name='addUser']").find("#txt_name").val().trim(),
			email    : jQuery("form[name='addUser']").find("#txt_email").val().trim().trim(),
			password : jQuery("form[name='addUser']").find("#txt_pass").val().trim().trim()
		}

		jQuery.ajax({
			url : ajaxurl,
			type: 'POST',
			data: data,
			success: function(successResult)
			{
				console.log("Here", successResult);
			},
			error: function(errorResult)
			{
				console.log("Error:", errorResult.responseText);
			}
		});
		return false;
	}
}


jQuery(document).ready(function(){

	// alert(ajaxurl);

	//events
	jQuery("form[name='addUser']").find(":submit").click(App.submitNewUser);

});