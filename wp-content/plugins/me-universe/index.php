<?php
	/*
    Plugin Name: My First WordPress Plugin
    Plugin URI: http://www.arefmirhoseini.net
    Description: Description goes here
    Author: Aref M.
    Version: 1.0
    Author URI: http://www.arefmirhoseini.net
    */

    add_action("wp_footer", "footer_msg");
    add_action("admin_menu", "admin_menu");

    function footer_msg()
    {
    	echo "Footer Message Goes Here!";
    }

    function admin_menu()
    {
    	add_options_page("My First WordPress Plugin", "Plugin-Users", 1, "users", "cb1");
    }

    function cb1()
    {
    	if ($_SERVER['REQUEST_METHOD'] === 'POST')
    	{
    		var_dump($_POST);
    		// saveUser($_POST);
    	} else {
	    	echo "<h1>Add New Users</h1>";
	    	echo "<form name='addUser' method='post'>
	    		<p><label for='txt_name'>Full Name</label><br />
	    		<input type='text' id='txt_name' name='txt_name' /></p>
	    		<p><label for='txt_email'>E-mail</label><br />
	    		<input type='text' id='txt_email' name='txt_email' /></p>
	    		<p><label for='txt_pass'>Password</label><br />
	    		<input type='password' id='txt_pass' name='txt_pass' /></p>
	    		<button type='submit'>Add User</button>
	    	</form><hr />
	    	<h1>Users List</h1><div id='userList'>";
	    	$users = getUsers();
	    	if (count($users))
	    	{
	    		echo "<ol>";
	    		foreach ($users as $user) 
	    		{
	    			echo "<li>" . $user->full_name . " (" . $user->email . ")</li>";
	    		}
	    		echo "</ol>";
	    	} else {
	    		echo "There isn't any user!";
	    	}
	    	echo "</div><script type='text/javascript'>
			    var ajaxurl = '" . admin_url('admin-ajax.php') . "';
			</script>";
	    	wp_register_script("myApp", plugins_url("js/app.js", __FILE__));
	    	wp_enqueue_script("myApp");
    	}
    }

    function saveUser($data)
    {
    	global $wpdb;
    	//Hash Password
    	$salt     = substr(sha1(time() * rand(1001, 1999)), 3, 5);	
		$passHash = sha1($salt . $data['txt_pass']) . $salt;
    	$data = array(
    		'full_name' => $data['txt_name'],
    		'email'     => $data['txt_email'],
    		'password'  => $passHash
    	);
    	$wpdb->insert('wp_subscribers', $data);
    	// return $wpdb->insert_id;
    }

    function getUsers()
    {
    	global $wpdb;
    	return $wpdb->get_results('select * from wp_subscribers order by id desc', OBJECT_K);
    }