<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'password');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+ ?~7/-{<RSt6_}rUx$oqBYGO~cG!Sc}&i(RDUjAU:*wyeU+yQ67xXHhCJ{;.?Y)');
define('SECURE_AUTH_KEY',  'Dj$lEc.v|6i(t:Z_;&v8nAw%}[V5vjRO`]B>2,`7v7_|D^6^|3)[-XQXl.PtpSbW');
define('LOGGED_IN_KEY',    '}C5I<z UR+YW`m,+0r2/;81F]|-y LNmy5%qTQKlU2sv*-O?C{]^V3[vTrf-pzti');
define('NONCE_KEY',        'w-D@Af)_XHr.^w16bGiaj001&V(Us=2qNO$wv5w`.xz7|IM}`$(X;D189-CX_D`~');
define('AUTH_SALT',        ' VA9d/A53L0b+E0E#:;Scp-[eiB&DJn-I:S5=$1*[yvalOuQ>ka$i!3mA4L7#e9]');
define('SECURE_AUTH_SALT', 'kg3bEuB<i=e.i#NPGUJ> L,2PS`.Myl}br:nw5<p(AQ~~!(PS&g4g: iuIa_>9y?');
define('LOGGED_IN_SALT',   '~yMP2w8h[C[;A1gVn[3iw&sz&T!5c8FKXwX(sVlyF|%RJ~To5o.GN&G [~O.m++|');
define('NONCE_SALT',       '-jvq9D||sYB@A+!u3[XG{%]; 4OsKdhF-/7Vg59>8;-3OTo*)9nz+yj;m|QE>?;0');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


